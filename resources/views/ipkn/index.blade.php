<table class="table custom table-striped fold-table">
    <thead>
        <tr style="background: linear-gradient(90deg, #2F5B9E 0%, #132B50 100%);color:#fff">
            <th style="width: 60px; text-align: center;">No</th>
            <th>Index Component</th>
            <th style="width: 165px; text-align: right">Score <?=date('Y')?></th>
            <th style="width: 30px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr class="view">
            <td style="text-align: center"></td>
            <td style="text-align: left; font-weight: bold; font-size: 16px">Indeks Pembangunan Kepariwisataan Nasional</td>
            <td style="width:165px; text-align: right"><?=get_ipkn_global_index('score')?></td>
            <td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
        </tr>

        <?php if($index){?>
        <?php $no=1;foreach($index as $i){?>
        <tr class="view" style="background: #122b5254">
            <td style="text-align: center"><?=$no++?></td>
            <td style="text-align: left"><?=$i->index_desc?></td>
            <td style="width:165px; text-align: right"><?=get_ipkn_score_index($i->index_id,'score')?> <span class="arrow-down-color" style="color: red"></span></td>
            <td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
        </tr>
        <?php $pillar = get_pillar_ipkn($i->index_id);?>
        <?php if($pillar){?>
        <?php foreach($pillar as $p){?>
            <tr class="view">
                <td style="text-align: center"></td>
                <td style="text-align: left">
                    <?=$p->pillar_desc?>
                </td>
                <td style="width:165px; text-align: right"><?=get_ipkn_score_pillar($p->pillar_id,'score')?></td>
                <td style="width:30px; text-align: right"><span class="table-view-dropdown fa fa-caret-down"></span></td>
            </tr>

            <tr class="fold">
                <td class="fold-area" colspan="5">
                    <div class="fold-content">
                        <table class="table custom">
                            <tbody>
                                <?php $sub_pillar = get_sub_pillar_ipkn($p->pillar_id)?>
                                <?php if($sub_pillar){?>
                                <?php foreach($sub_pillar as $sp){?>
                                <tr>
                                    <td style="width: 60px"></td>
                                    <td style="text-align: left;">
                                        <a class="info" style="cursor:pointer !important" onclick="show_modal(<?=$sp->indicator_id?>, 'Information')">
                                            <i class="fa fa-circle-info color-grey fs-20 color-grey fs-20"></i>
                                        </a>
                                        <?=$sp->indicator_code.' '.$sp->indicator_name?>
                                    </td>
                                    <?php
                                        $score = get_ipkn_score_sub_pillar($p->pillar_id,$sp->indicator_id,'score');
                                    ?>
                                    <td style="width:165px; text-align: right"><?=$score?> </td>
                                    <th style="width:30px; text-align: right">&nbsp;</th>
                                </tr>
                                <?php }?>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        <?php }?>
        <?php }?>
        <?php }?>
        <?php }?>
    </tbody>
</table>