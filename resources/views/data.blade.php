@extends('app')
@push('style')
@endpush

@section('content')
<section class="masthead text-white mb-5 mt-min-custom">
  <div class="container">
      <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! isset($section_1_header) ? nl2br($section_1_header->section_title) : '' !!}</h2>
      <p class="text-center font-inter fs-16 fw-normal color-grey mb-5">{!! isset($section_1_header) ? nl2br($section_1_header->section_subtitle) : '' !!}</p>
      <div class="row justify-content-center mb-5">
					<div class="col-md-6">
            <form method="post">
              <div class="row">
                <div class="col-md-8 mb-3">
                  <select class="form-select form-select-lg br-20 color-dark-navy fs-14" aria-label="Default select example">
                    <option value="">Pilih Kategori</option>
                    <?php if($category){?>
                    <?php foreach($category as $c){?>
                    <option value="<?=$c->value?>"><?=$c->label?></option>
                    <?php }?>
                    <?php }?>
                  </select>
                </div>
                <div class="col-md-4">
                  <select class="form-select form-select-lg br-20 color-dark-navy fs-14" aria-label="Default select example">
                    <option value="">Tahun</option>
                    <?php for($i=2019;$i<=date('Y');$i++){?>
                    <option value="<?=$i?>"><?=$i?></option>
                    <?php }?>
                  </select>
                </div>
              </div>
            </form>
					</div>
      </div>

      @if($rows)
      @foreach ($rows as $val)
      <div class="card mb-4">
          <div class="row g-0">
            <div class="col-md-2">
              <div class="padding-img-news">
                <div class="img-fluid">
                    <?php if($val->report_cover != NULL){?>
                    <img src="data:image/jpeg;base64,{{base64_encode($val->report_cover)}}" width="100%" height="240">
                    <?php } else {?>
                    <img src="<?=asset('assets/img/data.png')?>" width="100%" height="240">
                    <?php }?>
                </div>
                <?php if(get_attachment_data($val->id) != ""){?>
                <a class="btn" href="<?=get_attachment_data($val->id)?>" style="width:100%" download="<?=get_attachment_data_name($val->id)?>">
                  <i class="fa fa-download"></i> Download
                </a>
                <?php }?>
                <!--<select class="form-select br-20 color-dark-navy fs-14">
                  <option value="1">Laporan TTDI/TTCI</option>
                  <option value="2">Laporan TTDI/TTCI</option>
                </select>-->
              </div>
            </div>
            <div class="col-md-10">
              <div class="card-body">
              <h5 class="card-title line-clamp-3 color-dark-navy font-inter fs-20 fw-normal-2 mb-3">{{$val->title}}</h5>
              <p class="font-inter fs-16 mb-4 line-clamp-3 color-grey">{{$val->description}}</p>
              <div class="row align-items-end" style="height:8rem">
                <div class="col">
                  <table class="table table-borderless" style="margin-bottom:0px;">
                    <tr>
                      <td rowspan="2" class="text-left" style="vertical-align:middle;width:60px">
                        <img src="<?=asset('assets/img/logo_kemenparekraf_warna.png')?>" height="50">
                      </td>
                      <td colspan="2">
                        <small class="font-inter color-dark-navy fw-normal-2 fs-14 mb-1">{{$val->owner}}</small>
                      </td>
                      <td rowspan="2" style="vertical-align:middle;text-align:right">
                        <!--<small class="font-inter fw-normal-1 fs-14 color-dark-navy mb-1"><i class="fa fa-eye"></i> 23</small>-->
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <small class="font-inter color-grey fw-normal fs-12">{{ format_tanggal($val->updated_date) }}</small>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              </div>
            </div>
          </div>
      </div>
      @endforeach
      @endif

      <div class="container">
				<div class="row">
					<div class="col-md-12">
            <div class="row float-end-custom">
              {{ $rows->withQueryString()->onEachSide(0)->links() }}
            </div>
          </div>
        </div>
      </div>
  </div>
</section>
@endsection

@push('script')
@endpush
