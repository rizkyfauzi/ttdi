@if ($paginator->hasPages())
    <nav>
        <ul id="social-media" style="padding-left: 0;">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <!--<span aria-hidden="true">&lsaquo;</span>-->
                    <!--<button class="btn color-gold fw-bold" style="width: 40px;height: 40px;border-radius:50%;" type="button"><i class="fa fa-angle-left"></i></button>-->
                </li>
            @else
                <li>
                    <!--<a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>-->
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')" class="color-brown fw-bold">
                        <button class="btn color-gold fw-bold" style="width: 40px;height: 40px;border-radius:50%;" type="button"><i class="fa fa-angle-left"></i></button>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span class="color-brown fw-bold">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page"><span><button class="btn" style="width: 40px;
    height: 40px;border-radius:50%;background-color: #EAC170" type="button">{{ $page }}</button></span></li>
                        @else
                            <li><a href="{{ $url }}" class="color-brown fw-bold"><button class="btn color-brown fw-bold" style="width: 40px;
    height: 40px;border-radius:50%;" type="button">{{ $page }}</button></a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <!--<a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>-->
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')" class="color-brown fw-bold">
                        <button class="btn color-gold fw-bold" style="width: 40px;height: 40px;border-radius:50%;" type="button"><i class="fa fa-angle-right"></i></button>
                    </a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <!--<span aria-hidden="true">&rsaquo;</span>-->
                    <!--<button class="btn color-gold fw-bold" style="width: 40px;height: 40px;border-radius:50%;" type="button"><i class="fa fa-angle-right"></i></button>-->
                </li>
            @endif
        </ul>
    </nav>
@endif
