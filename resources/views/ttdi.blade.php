@extends('app')
@push('style')
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
@endpush

@section('content')
<?php if(isset($section_1_body) and $section_1_body->content_cover != NULL){?>
<section class="masthead" style="background-color:#132B50;background-image:url(data:image/jpeg;base64,{{base64_encode($section_1_body->content_cover)}});background-repeat:no-repeat;background-size:cover; "></section>
<?php }?>

<section class="py-4 mb-5" style="background-color:#132B50;">
  <div class="container">
    <h2 class="text-left font-rubik fw-bold color-gold fw-bold" style="margin-bottom:0px">{!! isset($section_1_header) ? nl2br($section_1_header->section_title) : '' !!}</h2>
  </div>
</section>

<section class="mb-5">
  <div class="container" data-aos="fade-down">
    <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! isset($section_11_header) ? nl2br($section_11_header->section_title) : '' !!}</h2>
    <p class="text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1" style="margin-bottom:0px">{!! isset($section_11_header) ? nl2br($section_11_header->section_subtitle) : '' !!}</p>
  </div>
</section>

<section class="text-white mb-5" style="overflow:hidden">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6 text-left" data-aos="fade-right">
        <p class="font-inter fw-normal-2 fs-20 color-dark-navy">{!! isset($section_11_body) ? nl2br($section_11_body->content_title) : '' !!}</p>
        <p class="font-inter fw-normal fs-16 color-grey">{!! isset($section_11_body) ? nl2br($section_11_body->content_description) : '' !!}</p>
      </div>
      <div class="col-md-6" data-aos="fade-left">
        <?php if(isset($section_11_body) and $section_11_body->content_cover != NULL){?>
        <img src="data:image/jpeg;base64,{{base64_encode($section_11_body->content_cover)}}" class="img-fluid float-end">
        <?php }?>
      </div>
    </div>
  </div>
</section>

<section class="page-section" style="background:#F1F5F9;overflow:hidden">
    <div class="container">
      <?php if(isset($section_2_header) and $section_2_header->section_title != NULL){?>
        <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! isset($section_2_header) ? nl2br($section_2_header->section_title) : '' !!}</h2>
        <p class="fst-italic text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1 mb-6">{!! isset($section_2_header) ? nl2br($section_2_header->section_subtitle) : '' !!}</p>
        <?php }?>
        <div class="show-big-banner">
          <div class="row justify-content-center mb-5">
            <?php if(get_image_content(6,4)){?>
            <?php foreach(get_image_content(6,4) as $k=>$r){?>
            <?php if($r->file_content != NULL){?>
            <div class="col-lg-2"><img src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="img-fluid w-100"></div>
            <?php }?>
            <?php }?>
            <?php }?>
            <?php if($section_2_body->content_cover != NULL){?>
            <div class="col-lg-3"><img src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="img-fluid w-100" style="margin-top:-72px"></div>
            <?php }?>
          </div>
        </div>
        <div class="hide-big-banner">
          <div class="row mb-3">
            <?php if(get_image_content(6,4)){?>
            <?php foreach(get_image_content(6,4) as $k=>$r){?>
            <?php if($r->file_content != NULL){?>
            <div class="col <?=($k == 0 or $k == 1) ? 'mb-4' : 'mb-2'?>"><img src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="img-fluid w-100"></div>
            <?php }?>
            <?php }?>
            <?php }?>
          </div>
          <div class="row mb-5 justify-content-center">
            <div class="col-md-5 text-center">
              <?php if(isset($section_2_body) and $section_2_body->content_cover != NULL){?>
              <img src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="img-fluid w-100">
              <?php }?>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-8" data-aos="fade-bottom">
            <h6 class="text-center mb-3 font-inter fs-20 fw-normal-2 font-rubik fw-bold color-navy"><?=isset($section_2_body) ? $section_2_body->content_title : ''?></h6>
            <p class="lead"><?=isset($section_2_body) ? $section_2_body->content_description : ''?></p>
          </div>
        </div>
    </div>
</section>

<section class="page-section text-white" style="overflow:hidden">
  <div class="container justify-content-center">
    <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! isset($section_3_header) ? nl2br($section_3_header->section_title) : '' !!}</h2>
    <p class="text-center mb-5 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_3_header) ? nl2br($section_3_header->section_subtitle) : '' !!}</p>
    <div class="row justify-content-center">
      <div class="col-lg-12 mb-lg-0 text-center">
        <ul id="pilar" style="padding-left: 0;">
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/enabling-environment.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Enabling<br> Environment</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/policy-conditions.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Travel & Tourism<br> Policy Conditions</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/infrastructure-new.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Infrastructure</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/demand-drivers.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Travel & Tourism<br> Demand Drivers</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/sustainability.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Travel & Tourism<br> Sustainability</p>
              </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="page-section"  style="background:#F1F5F9">
  <div class="container">
      <div class="row align-items-center">
        <div class="col-md-10">
          <h2 class="mb-2 font-rubik fw-bold color-navy">{!! isset($section_4_header) ? nl2br($section_4_header->section_title) : '' !!}</h2>
          <p class="mb-4 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_4_header) ? nl2br($section_4_header->section_subtitle) : 
            '' !!}</p>
        </div>
        <div class="col-md-2 mb-3">
          <!--<div class="dropdown float-end-custom">
            <button class="btn dropdown-toggle font-rubik fw-bold color-navy fs-14" style="background:#F1F5F9;border-color: #F1F5F9;border-radius:50px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            2021
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">2022</a>
            <a class="dropdown-item" href="#">2021</a>
            <a class="dropdown-item" href="#">2020</a>
            </div>
          </div>-->
        </div>
        <div class="table-responsive">
          <?php if($capaian_ttdi){?>
            <table class="table custom table-bordered table-striped table-responsive" width="100%">
              <thead>
                <tr>
                  <td style="background: #EAC170;"><b style="font-size:20px">Sub-Index</b></td>
                  <?php foreach($capaian_ttdi as $ct){?>
                  <td class="text-center" style="width:100px"><b><?=$ct->country_name?></b></td>
                  <?php }?>
                </tr>
              </thead>
              <tbody>
									<tr>
										<td><b>Enabling Environment</b></td>
                    <?php foreach($capaian_ttdi as $ct){?>
										<td class="text-center" <?=(get_capaian_ttdi_high_score('index_1')!=NULL and get_capaian_ttdi_high_score('index_1') == $ct->index_1) ? 'style="background: #EAC170;"' : ''?>><?=$ct->index_1?></td>
                    <?php }?>
									</tr>
                  <tr>
										<td><b>T&T Policy and Conditions</b></td>
                    <?php foreach($capaian_ttdi as $ct){?>
										<td class="text-center" <?=(get_capaian_ttdi_high_score('index_2')!=NULL and get_capaian_ttdi_high_score('index_2') == $ct->index_2) ? 'style="background: #EAC170;"' : ''?>><?=$ct->index_2?></td>
                    <?php }?>
									</tr>
                  <tr>
										<td><b>Infrastructure</b></td>
                    <?php foreach($capaian_ttdi as $ct){?>
										<td class="text-center" <?=(get_capaian_ttdi_high_score('index_3')!=NULL and get_capaian_ttdi_high_score('index_3') == $ct->index_3) ? 'style="background: #EAC170;"' : ''?>><?=$ct->index_3?></td>
                    <?php }?>
									</tr>
                  <tr>
										<td><b>T&T Demand Drivers</b></td>
                    <?php foreach($capaian_ttdi as $ct){?>
										<td class="text-center" <?=(get_capaian_ttdi_high_score('index_4')!=NULL and get_capaian_ttdi_high_score('index_4') == $ct->index_4) ? 'style="background: #EAC170;"' : ''?>><?=$ct->index_4?></td>
                    <?php }?>
									</tr>
                  <tr>
										<td><b>T&T Sustainability</b></td>
                    <?php foreach($capaian_ttdi as $ct){?>
										<td class="text-center" <?=(get_capaian_ttdi_high_score('index_5')!=NULL and get_capaian_ttdi_high_score('index_5') == $ct->index_5) ? 'style="background: #EAC170;"' : ''?>><?=$ct->index_5?></td>
                    <?php }?>
									</tr>
              </tbody>
            </table>
          <?php }?>
        </div>
      </div>
  </div>
</section>

<section class="page-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-5">
        <h2 class="font-rubik fw-bold color-navy">{!! isset($section_5_header) ? nl2br($section_5_header->section_title) : '' !!}</h2>
        <div class="dropdown">
          <button class="btn dropdown-toggle" style="background:#F1F5F9;font-weight:700;font-family:'Rubik';color: #132B50;border-color: #F1F5F9;border-radius:50px;font-size:14px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          2021
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="#">2020</a>
          <a class="dropdown-item" href="#">2019</a>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div id="chartTTDI" style="width: 100%;height:400px;font-size:10px"></div>
      </div>
    </div>
  </div>
</section>

<section class="text-white mb-5">
  <div class="container">
        <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! isset($section_6_header) ? nl2br($section_6_header->section_title) : '' !!}</h2>
				<p class="fst-italic text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_6_header) ? nl2br($section_6_header->section_subtitle) : '' !!}</p>
				<div class="dropdown text-center mb-5 ">
				  <button class="btn dropdown-toggle font-rubik fw-bold color-navy fs-14" style="background:#F1F5F9;border-color: #F1F5F9;border-radius:50px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					2022
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">2022</a>
            <a class="dropdown-item" href="#">2019</a>
            <a class="dropdown-item" href="#">2017</a>
				  </div>
				</div>
				<div class="table-responsive">
          @include('ttdi.index')
        </div>
  </div>
</section>

<!-- modal-->
<div class="modal fade" id="modal-content-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="text-title-modal">Info</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- modal information-->
            <div class="modal-body">
                <div id="modal-content-body-load-page"></div>
            </div>
            <!--    modal information-->
        </div>
    </div>
</div>
<!--modal-->
@endsection

@push('script')
<script src="<?=asset('js/jquery.min.js')?>"></script>
<script src="//cdn.amcharts.com/lib/4/core.js"></script>
<script src="//cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script>
		var chart = am4core.create("chartTTDI", am4charts.RadarChart);
		
		chart.data = <?=get_capaian_ttdi()?>;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "pillar";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    /* Create and configure series */
    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "pillar";
    series.name = "";
    series.strokeWidth = 2;
    series.bullets.push(new am4charts.CircleBullet());

    var circleBullet = series.bullets.push(new am4charts.CircleBullet());
    circleBullet.tooltipText = "Value: [bold]{value}[/]";
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{value}";
    labelBullet.label.dy = -20;

    chart.cursor= new am4charts.RadarCursor();
</script>
<script type="text/javascript">
  $(".fold-table tr.view").on("click", function(){
      $(this).toggleClass("open").next(".fold").toggleClass("open");
  });

  function show_modal(id, title){
    $('#text-title-modal').text(title);
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "{{ route('ttdi/information') }}",
      data: {id: id}
    }).done(function(response) {
      $('#modal-content-body-load-page').html(response);
    });
    $("#modal-content-view").modal('show');
  }
</script>
@endpush
