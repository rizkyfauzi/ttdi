@extends('app')
@push('style')
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
@endpush

@section('content')
<?php if(isset($section_1_body) and $section_1_body->content_cover != NULL){?>
<section class="masthead" style="background-color:#132B50;background-image:url(data:image/jpeg;base64,{{base64_encode($section_1_body->content_cover)}});background-repeat:no-repeat;background-size:cover; align-self: stretch;">
</section>
<?php }?>

<section class="py-4 mb-5" style="background-color:#132B50;">
  <div class="container">
      <h2 class="text-left font-rubik fw-bold color-gold fw-bold" style="margin-bottom:0px">{!! isset($section_1_header) ? nl2br($section_1_header->section_title) : '' !!}</h2>
  </div>
</section>

<section class="mb-5">
  <div class="container" data-aos="fade-down">
      <h2 class="text-center mb-2 font-rubik fw-bold color-navy fw-bold">{!! isset($section_2_header) ? nl2br($section_2_header->section_title) : '' !!}</h2>
  </div>
</section>

<?php if($section_2_body){?>
<?php foreach($section_2_body as $key=>$sb){?>
<section class="mb-5">
      <div class="container">
          <div class="row align-items-center" data-aos="fade-bottom">
              <?php if($key == 0){?>
                <div class="col-md-6 mb-3">
                  <div class="float-start" data-aos="fade-right">
                    <?php if($sb->content_cover != NULL){?>
                    <img src="data:image/jpeg;base64,{{base64_encode($sb->content_cover)}}" class="img-fluid">
                    <?php }?>
                  </div>
                </div>
                <div class="col-md-6 text-left" data-aos="fade-left">
                  <p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
                  <p class="font-inter fw-normal fs-16 color-grey"><?=$sb->content_description?></p>
                </div>
              <?php } else if($key == 1){?>
              <div class="col-md-6 mb-3 text-left" data-aos="fade-right">
                <p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
                <p class="font-inter fw-normal fs-16 color-grey"><?=$sb->content_description?></p>
              </div>
              <div class="col-md-6" data-aos="fade-left">
                <div class="float-end">
                  <?php if($sb->content_cover != NULL){?>
                  <img src="data:image/jpeg;base64,{{base64_encode($sb->content_cover)}}" class="img-fluid">
                  <?php }?>
                </div>
              </div>
              <?php }?>
          </div>
      </div>
</section>
<?php }?>
<?php }?>

<section class="page-section text-white" style="overflow:hidden">
  <div class="container justify-content-center">
    <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! isset($section_3_header) ? nl2br($section_3_header->section_title) : '' !!}</h2>
    <p class="text-center mb-5 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_3_header) ? nl2br($section_3_header->section_subtitle) : '' !!}</p>
    <div class="row justify-content-center">
      <div class="col-lg-12 mb-lg-0 text-center">
        <ul id="pilar" style="padding-left: 0;">
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/enabling-environment.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Enabling<br> Environment</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/policy-conditions.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Travel & Tourism<br> Policy Conditions</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/infrastructure-new.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Infrastructure</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/demand-drivers.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Travel & Tourism<br> Demand Drivers</p>
              </div>
          </li>
          <li>
              <div class="text-center" data-aos="flip-up">
                <img src="<?=asset('assets/img/ttdi/sustainability.png')?>">
                <p class="fst-italic font-rubik fw-bold color-navy font-inter fs-16 fw-bold py-2">Travel & Tourism<br> Sustainability</p>
              </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="page-section"  style="background:#F1F5F9">
  <div class="container">
      <div class="row">
        <div class="col-md-10">
          <h2 class="mb-2 font-rubik fw-bold color-navy">{!! isset($section_4_header) ? nl2br($section_4_header->section_title) : '' !!}</h2>
          <p class="mb-4 font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1">{!! isset($section_4_header) ? nl2br($section_4_header->section_subtitle) : '' !!}</p>
        </div>
        <div class="col-md-2 mb-3">
          <!--<div class="dropdown float-end-custom">
            <button class="btn dropdown-toggle font-rubik fw-bold color-navy fs-14" style="background:#F1F5F9;border-color: #F1F5F9;border-radius:50px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            2021
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">2022</a>
            <a class="dropdown-item" href="#">2021</a>
            <a class="dropdown-item" href="#">2020</a>
            </div>
          </div>-->
        </div>
        <div class="col-md-12 text-left">
          <div class="table-responsive">
            <?php if($capaian_ipkn){?>
              <table class="table custom table-bordered table-striped" width="100%">
								<thead>
									<tr>
										<td class="text-center" style="width:30px"><b>No</b></td>
										<td class="text-center" style="width:100px"><b>Provinsi</b></td>
										<td class="text-center" style="width:100px"><b>Ranking</b></td>
										<td class="text-center" style="width:100px"><b>IPKN</b></td>
										<td class="text-center" style="width:100px"><b>Enabling Environment</b></td>
										<td class="text-center" style="width:100px"><b>T&amp;T Policy and Conditions</b></td>
										<td class="text-center" style="width:100px"><b>Infrastructure</b></td>
										<td class="text-center" style="width:100px"><b>T&amp;T Demand Drivers</b></td>
										<td class="text-center" style="width:100px"><b>T&amp;T Sustainability</b></td>
									</tr>
								</thead>
                <tbody>
                  <?php $no=1;foreach($capaian_ipkn as $ci){?>
									<tr>
										<td class="text-center"><?=$no++?></td>
										<td class="text-left"><?=$ci->name?></td>
										<td class="text-center" style="background: #EAC170;"><?=$ci->rank?></td>
										<td class="text-center"><?=$ci->score_ipkn?></td>
										<td class="text-center"><?=$ci->index_1?></td>
										<td class="text-center"><?=$ci->index_2?></td>
										<td class="text-center"><?=$ci->index_3?></td>
										<td class="text-center"><?=$ci->index_4?></td>
										<td class="text-center"><?=$ci->index_5?></td>
									</tr>
                  <?php }?>
                </tbody>
              </table>
            <?php }?>
          </div>
        </div>
      </div>
  </div>
</section>

<section class="page-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-5">
        <h2 class="font-rubik fw-bold color-navy">{!! isset($section_5_header) ? nl2br($section_5_header->section_title) : '' !!}</h2>
        <div class="dropdown">
          <button class="btn dropdown-toggle" style="background:#F1F5F9;font-weight:700;font-family:'Rubik';color: #132B50;border-color: #F1F5F9;border-radius:50px;font-size:14px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Bali
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="#">Jakarta</a>
          <a class="dropdown-item" href="#">Bandung</a>
          <a class="dropdown-item" href="#">Bali</a>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div id="chartIPKN" style="width: 100%;height:400px;font-size:10px"></div>
      </div>
    </div>
  </div>
</section>

<section class="text-white mb-5">
  <div class="container">
      <h2 class="text-center mb-5 font-rubik fw-bold color-navy">{!! isset($section_6_header) ? nl2br($section_6_header->section_title) : '' !!}</h2>
      <!--<div class="dropdown text-center mb-5 ">
        <button class="btn dropdown-toggle font-rubik fw-bold color-navy fs-14" style="background:#F1F5F9;border-color: #F1F5F9;border-radius:50px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        2021
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="#">2022</a>
        <a class="dropdown-item" href="#">2021</a>
        <a class="dropdown-item" href="#">2020</a>
        </div>
      </div>-->
      <div class="table-responsive">
        @include('ipkn.index')
      </div>
  </div>
</section>

<!-- modal-->
<div class="modal fade" id="modal-content-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="text-title-modal">Info</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- modal information-->
            <div class="modal-body">
                <div id="modal-content-body-load-page"></div>
            </div>
            <!--    modal information-->
        </div>
    </div>
</div>
<!--modal-->
@endsection

@push('script')
<script src="<?=asset('js/jquery.min.js')?>"></script>
<script src="//cdn.amcharts.com/lib/4/core.js"></script>
<script src="//cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script>
		var chart = am4core.create("chartIPKN", am4charts.RadarChart);
		
		chart.data = <?=get_capaian_ipkn()?>;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "pillar";

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    /* Create and configure series */
    var series = chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "pillar";
    series.name = "";
    series.strokeWidth = 2;
    series.bullets.push(new am4charts.CircleBullet());

    var circleBullet = series.bullets.push(new am4charts.CircleBullet());
    circleBullet.tooltipText = "Value: [bold]{value}[/]";
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{value}";
    labelBullet.label.dy = -20;

    chart.cursor= new am4charts.RadarCursor();
</script>
<script type="text/javascript">
  $(".fold-table tr.view").on("click", function(){
      $(this).toggleClass("open").next(".fold").toggleClass("open");
  });

  function show_modal(id, title){
    $('#text-title-modal').text(title);
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: "POST",
      url: "{{ route('ipkn/information') }}",
      data: {id: id}
    }).done(function(response) {
      $('#modal-content-body-load-page').html(response);
    });
    $("#modal-content-view").modal('show');
  }
</script>
@endpush
