@extends('app')
@push('style')
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
@endpush

@section('content')
		<?php if(isset($section_banner_body) and $section_banner_body->content_cover != NULL){?>
		<section class="mb-5" style="padding-top: calc(6rem + 32px);overflow:hidden;background-image:url(data:image/jpeg;base64,{{base64_encode($section_banner_body->content_cover)}}) ;background-position:right;background-repeat:no-repeat;height:640px;background-color:#132B50;">
			<div class="container text-left py-5 ps-4">
				<div class="row">
					<div class="col-md-6">
						<ul id="social-media" style="padding-left: 0;">
							<li class="mb-2"><img src="assets/img/hut_ri_77_logo.png" style="vertical-align:bottom" width="80px"></li>
							<li class="mb-2"><img src="assets/img/g20_logo.png" width="60px"></li>
						</ul>
						<p class="font-rubik fw-normal fs-16 text-white">{!! nl2br($section_banner->section_title) !!}</p>
						<h1 class="font-rubik fw-normal-2 fs-36 color-gold">{!! nl2br($section_banner->section_subtitle) !!}</h1>
					</div>
				</div>
			</div>
		</section>
		<?php }?>
		
		<?php if($section_1_header->section_title != NULL){?>
		<section class="mb-5">
            <div class="container" data-aos="fade-down">
                <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_1_header->section_title) !!}</h2>
				<p class="text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1 mb-6">{!! nl2br($section_1_header->section_subtitle) !!}</p>
			</div>
		</section>
		<?php }?>
		
		<?php if($section_1_body){?>
		<?php foreach($section_1_body as $key=>$sb){?>
		<section class="mb-5">
            <div class="container">
                <div class="row align-items-center" data-aos="fade-bottom">
					<?php if($key == 0){?>
					<div class="col-md-7 text-left">
						<p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
						<p class="font-inter fw-normal fs-16 color-grey mb-5"><?=$sb->content_description?></p>
					</div>
					<div class="col-md-5">
						<div class="row float-end mb-5">
							<?php if(get_image_content($sb->content_id,2)){?>
							<?php foreach(get_image_content($sb->content_id,2) as $k=>$r){?>
								<?php if($r->file_content != NULL){?>
							<div class="col <?=($k == 0) ? 'mb-3' : ''?>" <?=($k == 0) ? ' style="padding-right:0px"' : ''?>>
								<img src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="img-fluid w-100" <?=($k == 0) ? ' style="margin-top:-30px"' : ''?>>
							</div>
							<?php }?>
							<?php }?>
							<?php }?>
						</div>
					</div>
					<?php } else if($key == 1){?>
					<div class="col-md-5">
						<div class="row float-start mb-3">
							<?php if(get_image_content($sb->content_id,2)){?>
							<?php foreach(get_image_content($sb->content_id,2) as $k=>$r){?>
							<?php if($r->file_content != NULL){?>
							<div class="col <?=($k == 0) ? 'mb-3' : ''?>" <?=($k == 0) ? 'style="padding-right:0px"' : ''?>>
								<img src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="img-fluid w-100" <?=($k == 1) ? 'style="margin-top:-30px"' : ''?>>
							</div>
							<?php }?>
							<?php }?>
							<?php }?>
						</div>
					</div>
					<div class="col-md-7 text-left">
						<p class="font-inter fw-normal-2 fs-20 color-dark-navy"><?=$sb->content_title?></p>
						<p class="font-inter fw-normal fs-16 color-grey"><?=$sb->content_description?></p>
					</div>
					<?php }?>
				</div>
			</div>
		</section>
		<?php }?>
		<?php }?>		
		
		<!-- Transformasi Section-->
        <section class="page-section" style="background:#F1F5F9;overflow:hidden">
            <div class="container">
				<?php if(isset($section_2_header) and $section_2_header->section_title != NULL){?>
                <h2 class="text-center mb-2 font-rubik fw-bold color-navy">{!! nl2br($section_2_header->section_title) !!}</h2>
				<p class="fst-italic text-center font-rubik fw-bold color-navy font-inter fs-16 fw-normal-1 mb-6">{!! nl2br($section_2_header->section_subtitle) !!}</p>
                <?php }?>
                <div class="show-big-banner">
					<div class="row justify-content-center mb-5">
						<?php if(get_image_content(6,4)){?>
						<?php foreach(get_image_content(6,4) as $k=>$r){?>
						<?php if($r->file_content != NULL){?>
						<div class="col-lg-2"><img src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="img-fluid w-100"></div>
						<?php }?>
						<?php }?>
						<?php }?>
						<?php if($section_2_body->content_cover != NULL){?>
						<div class="col-lg-3"><img src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="img-fluid w-100" style="margin-top:-72px"></div>
						<?php }?>
					</div>
                </div>
				<div class="hide-big-banner">
					<div class="row mb-3">
						<?php if(get_image_content(6,4)){?>
						<?php foreach(get_image_content(6,4) as $k=>$r){?>
						<?php if($r->file_content != NULL){?>
						<div class="col <?=($k == 0 or $k == 1) ? 'mb-4' : 'mb-2'?>"><img src="data:image/jpeg;base64,{{base64_encode($r->file_content)}}" class="img-fluid w-100"></div>
						<?php }?>
						<?php }?>
						<?php }?>
					</div>
					<div class="row mb-5 justify-content-center">
						<div class="col-md-5 text-center">
							<?php if($section_2_body->content_cover != NULL){?>
							<img src="data:image/jpeg;base64,{{base64_encode($section_2_body->content_cover)}}" class="img-fluid w-100">
							<?php }?>
						</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-8" data-aos="fade-bottom">
						<h6 class="text-center mb-3 font-inter fs-20 fw-normal-2 font-rubik fw-bold color-navy"><?=$section_2_body->content_title?></h6>
						<p class="lead"><?=$section_2_body->content_description?></p>
					</div>
                </div>
            </div>
        </section>
		
		<section class="page-section">
            <div class="container justify-content-center">
                <div class="row align-items-center">
					<div class="col-md-6">
						<h2 class="font-rubik fw-bold color-navy">{!! (isset($section_3_header)) ? nl2br($section_3_header->section_title) : '' !!}</h2>
						<div class="dropdown">
						  <button class="btn dropdown-toggle font-rubik fw-bold color-navy fs-14" style="background:#F1F5F9;border-color: #F1F5F9;border-radius:50px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							2021
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">2022</a>
							<a class="dropdown-item" href="#">2021</a>
							<a class="dropdown-item" href="#">2020</a>
						  </div>
						</div>
					</div>
					<div class="col-md-6">
						<div id="chartdiv2" style="width: 100%;height:400px"></div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="page-section"  style="background:#F1F5F9;overflow:hidden">
            <div class="container">
                <div class="row">
					<div class="col-md-6 mb-3" data-aos="fade-right">
						<?php if(isset($section_4_body) and $section_4_body->content_cover != NULL){?>
						<img src="data:image/jpeg;base64,{{base64_encode($section_4_body->content_cover)}}" class="img-fluid float-end">
						<?php }?>
					</div>
					<div class="col-md-6 text-left" data-aos="fade-left">
						<h2 class="text-left mb-2 font-rubik fw-bold color-navy">{!! isset($section_4_body) ? nl2br($section_4_body->content_title) : '' !!}</h2>
						<p class="font-inter fs-20 fw-normal-2 color-dark-navy">{!! isset($section_4_body) ? nl2br($section_4_body->content_subtitle) : '' !!}</p>
						<p class="font-inter fs-16 fw-normal color-grey">{{ isset($section_4_body) ? $section_4_body->content_description : '' }}</p>
						<a class="btn btn-lg font-rubik fw-normal-1 color-gold fs-14" href="{{ route('ipkn') }}" style="background:#1E293B;border-radius:50px;">Selengkapnya&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a>
                    </a>
					</div>
				</div>
			</div>
		</section>
		
		<section class="page-section">
            <div class="container justify-content-center">
                <div class="row align-items-center">
					<div class="col-md-6">
						<h2 class="font-rubik fw-bold color-navy">{!! isset($section_5_header) ? nl2br($section_5_header->section_title) : '' !!}</h2>
						<div class="dropdown">
						  <button class="btn dropdown-toggle" style="background:#F1F5F9;font-weight:700;font-family:'Rubik';color: #132B50;border-color: #F1F5F9;border-radius:50px;font-size:14px;" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Bali
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">Jakarta</a>
							<a class="dropdown-item" href="#">Bandung</a>
							<a class="dropdown-item" href="#">Bali</a>
						  </div>
						</div>
					</div>
					<div class="col-md-6">
						<div id="chartdiv" style="width: 100%;height:400px"></div>
						<!--<img src="assets/img/banner/home-banner.png" class="img-responsive w-100" style="float:right">-->
					</div>
				</div>
			</div>
		</section>
		
		<!-- Berita Section-->
        <section class="text-white mb-5">
            <div class="container">
                <h2 class="text-center mb-5 font-rubik fw-bold color-navy fw-bold">{!! isset($section_6_header) ? nl2br($section_6_header->section_title) : '' !!}</h2>
				@if($berita)
				@foreach ($berita as $val)
				<div class="card mb-4">
					<a href="{{ route('berita/detail',[$val->content_id]) }}">
						<div class="row g-0">
						<div class="col-md-4">
							<div class="img-fluid" >
								<img src="data:image/jpeg;base64,{{base64_encode($val->content_cover)}}" class="padding-img-news" width="100%" height="240">
							</div>
						</div>
						<div class="col-md-8">
							<div class="card-body">
							<h5 class="card-title line-clamp-3 color-dark-navy font-inter fs-20 fw-normal-2 mb-3">{{ $val->content_title }}</h5>
							<div class="font-inter fs-16 line-clamp-3 color-grey">{!! $val->content_description !!}</div>
							<div class="row align-items-end" style="height:4.3rem">
								<div class="col">
									<table class="table table-borderless" style="margin-bottom:0">
										<tr>
											<td rowspan="2" class="text-left" style="vertical-align:middle;width:60px">
												<img src="assets/img/logo_kemenparekraf_warna.png" height="50">
											</td>
											<td colspan="2">
												<small class="font-inter color-dark-navy fw-normal-2 fs-14 mb-1">{{ $val->content_owner }}</small>
											</td>
											<td rowspan="2" style="vertical-align:middle;text-align:right">
												<!--<small class="font-inter fw-normal-1 fs-14 color-dark-navy mb-1"><i class="fa fa-eye"></i> {{ $val->content_view_count }}</small>-->
											</td>
										</tr>
										<tr>
											<td>
												<small class="font-inter color-grey fw-normal fs-12">{{ format_tanggal($val->content_publish_date) }}</small>
											</td>
										</tr>
									</table>
								</div>
							</div>
							</div>
						</div>
						</div>
					</a>
				</div>
				@endforeach
				@endif
			</div>
			<div class="container text-center">
				<a class="btn btn-lg fw-bold font-rubik color-navy fs-14" href="{{ route('berita') }}" style="background:#fff;border-color: #64748B;border-radius:50px;">Cek Berita Lainnya</a>
            </div>
        </section>
		
@endsection

@push('script')
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();
</script>
<script type="text/javascript">
    
</script>
@endpush
