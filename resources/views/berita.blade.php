@extends('app')
@push('style')
@endpush

@section('content')
<section class="masthead text-white mb-5 mt-min-custom">
    <div class="container">
        <h2 class="text-center mb-5 font-rubik fw-bold color-navy fw-bold">{!! isset($section_1_header) ? nl2br($section_1_header->section_title) : '' !!}</h2>
        <div class="row justify-content-center mb-5">
            <div class="col-md-7">
                <form method="post">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <select class="form-select form-select-lg br-20 color-dark-navy fs-14" aria-label="Default select example">
                                <option value="">Semua Kategori</option>
                                <?php if($category){?>
                                <?php foreach($category as $c){?>
                                <option value="<?=$c->value?>"><?=$c->label?></option>
                                <?php }?>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa fa-search"></i></span>
                                <input type="text" name="search_field" class="form-control font-inter fs-14" placeholder="Cari berita" aria-label="" aria-describedby="basic-addon1">
                                <button class="btn btn-outline-secondary color-gold col-3" style="border-radius:20px;margin-left:-20px;" type="submit" id="button-addon2">Cari</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @if($rows)
        @foreach ($rows as $val)
        <div class="card mb-4">
            <a href="{{ route('berita/detail',[$val->content_id]) }}">
                <div class="row g-0">
                <div class="col-md-4">
                    <div class="img-fluid" >
                        <img src="data:image/jpeg;base64,{{base64_encode($val->content_cover)}}" class="padding-img-news" width="100%" height="240">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                    <h5 class="card-title line-clamp-3 color-dark-navy font-inter fs-20 fw-normal-2 mb-3">{{ $val->content_title }}</h5>
                    <div class="font-inter fs-16 line-clamp-3 color-grey">{!! $val->content_description !!}</div>
                    <div class="row align-items-end" style="height:4.3rem">
                        <div class="col">
                            <table class="table table-borderless" style="margin-bottom:0">
                                <tr>
                                    <td rowspan="2" class="text-left" style="vertical-align:middle;width:60px">
                                        <img src="assets/img/logo_kemenparekraf_warna.png" height="50">
                                    </td>
                                    <td colspan="2">
                                        <small class="font-inter color-dark-navy fw-normal-2 fs-14 mb-1">{{ $val->content_owner }}</small>
                                    </td>
                                    <td rowspan="2" style="vertical-align:middle;text-align:right">
                                        <!--<small class="font-inter fw-normal-1 fs-14 color-dark-navy mb-1"><i class="fa fa-eye"></i> {{ $val->content_view_count }}</small>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small class="font-inter color-grey fw-normal fs-12">{{ format_tanggal($val->content_publish_date) }}</small>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </a>
        </div>
        @endforeach
        @endif

      <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="row float-end-custom">
              {{ $rows->withQueryString()->onEachSide(0)->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection

@push('script')
@endpush
