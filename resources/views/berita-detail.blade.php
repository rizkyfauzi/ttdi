@extends('app')
@push('style')
@endpush

@section('content')
<?php if($row->content_cover != NULL){?>
<section class="show-big-banner" style="padding-top:10.5rem">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center br-20">
                <div style="height:380px;background-size:cover;background-position:center;overflow:hidden">
                    <a onclick="show_img({{ base64_encode($row->content_cover) }})"><img src="data:image/jpeg;base64,{{base64_encode($row->content_cover)}}" width="100%"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php }?>

<section class="page-section hide-big-banner" style="background-color:#132B50;background-image:url(data:image/jpeg;base64,{{base64_encode($row->content_cover)}});background-repeat:no-repeat;background-size:cover; align-self: stretch;"></section>

<section class="text-white mb-5 mb-min">
    <div class="container">
        <div class="row">
            <div class="col-md-1 show-big-banner">
                <!--<ul id="share" class="float-end" style="padding-left:0;list-style:none;margin-top:7rem">
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn color-navy" style="width: 50px;
height: 50px;border-radius:50%;background:#F1F5F9" type="button"><i class="fa fa-regular fa-heart"></i></button>
                        </a>
                    </li>
                    <li class="text-center">
                        <span class="color-grey">23</span>							
                    </li>
                    <li></li>
                    <li></li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 50px;
height: 50px;border-radius:50%;background:#132B50" type="button"><i class="fa fa-link"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 50px;
height: 50px;border-radius:50%;background:#427CF0" type="button"><i class="fa fa-brands fa-facebook-f"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 50px;
height: 50px;border-radius:50%;background:#EAC170" type="button"><i class="fa fa-envelope"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 50px;
height: 50px;border-radius:50%;background:#1DA1F2" type="button"><i class="fa fa-brands fa-twitter"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 50px;
height: 50px;border-radius:50%;background:#25D366" type="button"><i class="fa fa-brands fa-whatsapp"></i></button>
                        </a>
                    </li>
                </ul>-->
            </div>
            <div class="col-md-10">
                <div class="card mb-4" style="padding:10px;">
                    <div class="card-body">
                    <h5 class="card-title color-dark-navy font-inter fs-20 fw-normal-2 mb-5" style="line-height:30px">{{ $row->content_title }}</h5>
                    <div class="font-inter fs-16 color-grey mb-5">
                        {!! $row->content_description !!}
                    </div>
                    
                    <table class="table table-borderless" style="width:100%;padding:0px;margin-bottom:0">
                        <tr>
                            <td rowspan="2" class="text-left" style="vertical-align:middle;width:60px">
                                <img src="<?=asset('assets/img/logo_kemenparekraf_warna.png')?>" height="50">
                            </td>
                            <td colspan="2">
                                <small class="font-inter color-dark-navy fw-normal-2 fs-14 mb-1">{{ $row->content_owner }}</small>
                            </td>
                            <td rowspan="2" style="vertical-align:middle;text-align:right">
                                <!--<small class="font-inter fw-normal-1 fs-14 color-dark-navy mb-1"><i class="fa fa-eye"></i> {{ $row->content_view_count }}</small>-->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <small class="font-inter color-grey fw-normal fs-12">{{ format_tanggal($row->content_publish_date) }}</small>
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-md-1 hide-big-banner">
                <!--<ul id="share-end" style="padding-left:0;list-style:none;">
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn color-navy" style="width: 40px;
height: 40px;border-radius:50%;background:#F1F5F9" type="button"><i class="fa fa-regular fa-heart fs-14"></i></button>
                        </a>
                        <span class="color-grey">2</span>
                    </li>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                    <li>&nbsp;</li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 40px;
height: 40px;border-radius:50%;background:#132B50" type="button"><i class="fa fa-link fs-12"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white align-items-center" style="width: 40px;
height: 40px;border-radius:50%;background:#427CF0" type="button"><i class="fa fa-brands fa-facebook-f"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 40px;
height: 40px;border-radius:50%;background:#EAC170" type="button"><i class="fa fa-envelope fs-13"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 40px;
height: 40px;border-radius:50%;background:#1DA1F2" type="button"><i class="fa fa-brands fa-twitter"></i></button>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <button class="btn text-white" style="width: 40px;
height: 40px;border-radius:50%;background:#25D366" type="button"><i class="fa fa-brands fa-whatsapp"></i></button>
                        </a>
                    </li>
                </ul>-->
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h5 class="card-title color-dark-navy font-inter fs-20 fw-normal-2 text-left">Berita Terkait</h5>
                <div class="row">
                    @if($berita_terkait)
                    @foreach ($berita_terkait as $val)
                    <div class="col-md-4">
                        
                        <div class="card custom" style="border:none">
                            <img class="card-img-top" src="data:image/jpeg;base64,{{base64_encode($val->content_cover)}}">
                            <div class="card-body" style="padding-left:0;padding-right:0">
                                <a href="{{ route('berita/detail',[$val->content_id]) }}">
                                    <h5 class="card-title fs-16 color-navy mb-3">{{ $val->content_title }}</h5>
                                </a>
                                <table class="table table-borderless" style="width:100%;padding:0px;margin-bottom:0">
                                    <tr>
                                        <td rowspan="2" class="text-left" style="vertical-align:middle;width:50px">
                                            <img src="<?=asset('assets/img/logo_kemenparekraf_warna.png')?>" height="40">
                                        </td>
                                        <td colspan="2">
                                            <small class="font-inter fw-normal-2 fs-12 color-dark-navy mb-1">{{ $val->content_owner }}</small>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <small class="font-inter fw-normal color-grey fs-10">{{ format_tanggal($val->content_publish_date) }}</small>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
</section>
@endsection

@push('script')
<script type="text/javascript">
    function show_img (data) {
        var image = new Image();
        image.src = "data:image/jpg;base64,"+ data;

        var w = window.open("");
        w.document.write(image.outerHTML);
    }
</script>
@endpush
