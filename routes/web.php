<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\TTDIController;
use App\Http\Controllers\IPKNController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\BeritaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BerandaController::class, 'index'])->name('beranda');
Route::get('/ttdi', [TTDIController::class, 'index'])->name('ttdi');
Route::post('/ttdi/information', [TTDIController::class, 'information'])->name('ttdi/information');
Route::get('/ipkn', [IPKNController::class, 'index'])->name('ipkn');
Route::post('/ipkn/information', [IPKNController::class, 'information'])->name('ipkn/information');
Route::get('/data', [DataController::class, 'index'])->name('data');
Route::get('/berita', [BeritaController::class, 'index'])->name('berita');
Route::get('/berita/detail/{id}', [BeritaController::class, 'detail'])->name('berita/detail');
