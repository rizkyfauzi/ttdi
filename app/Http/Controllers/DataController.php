<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data;
use App\Models\Section;
use App\Models\GlobalParameter;

class DataController extends Controller
{
    public function index(Request $request){
		$section_1_header = Section::where('is_active','Y')->where('section_class','data')->where('section_view_name','section_1_view')->first();

		$rows = Data::where('is_active','Y')->orderBy('created_date','DESC')->paginate(5);
		$category = GlobalParameter::where('flag','kategori_data')->get();

		return view('data',compact('section_1_header','rows','category'));
	}
}
