<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Section;
use DB;

class TTDIController extends Controller
{
    public function index(Request $request){
		$section_1_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_1_view')->first();
		$section_1_body = Content::where('is_active','Y')->where('section_id',8)->orderBy('content_id','asc')->first();

		$section_11_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_2_view')->first();
		$section_11_body = Content::where('is_active','Y')->where('section_id',19)->orderBy('content_id','asc')->first();

		$section_2_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_2_view')->first();
		$section_2_body = Content::where('is_active','Y')->where('section_id',3)->orderBy('content_id','asc')->first();

		$section_3_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_3_view')->first();
		$section_4_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_4_view')->first();
		$section_5_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_5_view')->first();
		$section_6_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_6_view')->first();

		$capaian_ttdi = DB::table('cms_capaian_ttdi')->where('cms_capaian_ttdi.year',date('Y'))->get();

		$index = DB::table('mst_index')->where('is_active','Y')->get();

		return view('ttdi',compact('section_1_header','section_1_body','section_11_header','section_11_body','section_2_header','section_2_body','section_3_header','section_4_header','section_5_header','section_6_header','capaian_ttdi','index'));
	}

	public function information(Request $request){
		$id = $request->id;

		$row = DB::table('mst_subpillar')->where('subpillar_id',$id)->first();

		$html = '
				<table class="table table-borderless">
					<tbody>
						<tr>
							<td colspan="3"><span style="font-weight: bold">'.$row->subpillar_desc.'</span></td>
						</tr>
						<tr>
							<td width="200px">Deskripsi</td>
							<td width="10px">:</td>
							<td>'.$row->question.'</td>
						</tr>
					
						<tr>
							<td>Sumber</td>
							<td>:</td>
							<td>'.$row->source.'</td>
						</tr>
					
						<tr>
							<td>Edisi</td>
							<td>:</td>
							<td>'.$row->year_label.'</td>
						</tr>
					
						<tr>
							<td>Catatan</td>
							<td>: </td>
							<td>'.nl2br($row->note).'</td>
						</tr>
						<tr>
							<td>Tipe Data</td>
							<td>:</td>
							<td>'.$row->type_data.'</td>
						</tr>
						<tr>
							<td>Nilai Sebelumnya</td>
							<td>:</td>
							<td>'.$row->data_value.'</td>
						</tr>
					
						<tr>
							<td>Nilai Saat Ini</td>
							<td>:</td>
							<td>'.$row->weighted.'</td>
						</tr>
					
						<tr>
							<td>Statistik Saat Ini</td>
							<td>:</td>
							<td>Min '.$row->min_value.'; Max '.$row->max_value.'; Median '.$row->med_value.'</td>
						</tr>
					
						<tr>
							<td>Exclussion Filter</td>
							<td>:</td>
							<td>'.(($row->is_exclusive == "N") ? 'No' : 'Yes').'</td>
						</tr>
					
						<tr>
							<td>Outlayer</td>
							<td>:</td>
							<td>'.(($row->is_outlayer == "N") ? 'No' : 'Yes').'</td>
						</tr>
				
					</tbody>
				</table>
				';

		return $html;
	}
}
