<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Section;

class BerandaController extends Controller
{
    public function index(Request $request){
		$section_banner = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_banner')->first();
		$section_banner_body = Content::where('is_active','Y')->where('section_id',20)->orderBy('content_id','desc')->first();

		$section_1_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_1_view')->first();
		$section_1_body = Content::where('is_active','Y')->where('section_id',2)->orderBy('content_id','asc')->get();

		$section_2_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_2_view')->first();
		$section_2_body = Content::where('is_active','Y')->where('section_id',3)->orderBy('content_id','asc')->first();

		$section_3_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_3_view')->first();
		$section_4_body = Content::where('is_active','Y')->where('section_id',5)->orderBy('content_id','asc')->first();
		$section_5_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_5_view')->first();
		$section_6_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_6_view')->first();

		$berita = Content::where('is_active','Y')->where('content_type',1)->orderBy('content_publish_date','DESC')->limit(3)->get();

		return view('beranda',compact('section_banner','section_banner_body','section_1_header','section_1_body','section_2_header','section_2_body','section_3_header','section_4_body','section_5_header','section_6_header','berita'));
	}
}
