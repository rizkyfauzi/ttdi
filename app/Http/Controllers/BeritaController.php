<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Section;
use App\Models\GlobalParameter;
use DB;

class BeritaController extends Controller
{
    public function index(Request $request){
		$section_1_header = Section::where('is_active','Y')->where('section_class','beranda')->where('section_view_name','section_6_view')->first();

		$rows = Content::where('is_active','Y')->where('content_type',1)->orderBy('content_publish_date','DESC')->paginate(5);
		$category = GlobalParameter::where('flag','kategori_berita')->get();

		return view('berita',compact('section_1_header','rows','category'));
	}
	
	public function detail(Request $request,$id){
		$row = Content::where('content_id',$id)->first();
		$berita_terkait = Content::where('is_active','Y')->where('content_type',1)->whereNotIn('content_id',[$id])->orderBy(DB::raw('RAND()'))->take(3)->get();

		return view('berita-detail',compact('row','berita_terkait'));
	}
}
