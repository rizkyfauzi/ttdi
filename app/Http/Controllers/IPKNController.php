<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Section;
use DB;

class IPKNController extends Controller
{
    public function index(Request $request){
		$section_1_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_1_view')->first();
		$section_1_body = Content::where('is_active','Y')->where('section_id',13)->orderBy('content_id','asc')->first();

		$section_2_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_2_view')->first();
		$section_2_body = Content::where('is_active','Y')->where('section_id',14)->orderBy('content_id','asc')->limit(2)->get();

		$section_3_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_3_view')->first();
		$section_4_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_4_view')->first();
		$section_5_header = Section::where('is_active','Y')->where('section_class','ttdi')->where('section_view_name','section_5_view')->first();
		$section_6_header = Section::where('is_active','Y')->where('section_class','ipkn')->where('section_view_name','section_6_view')->first();

		$capaian_ipkn = DB::table('cms_capaian_ipkn')->join('mst_provinces','mst_provinces.id','=','cms_capaian_ipkn.province_id')->where('cms_capaian_ipkn.year',date('Y'))->get();

		$index = DB::table('ipkn_mst_subindex')->where('is_active','Y')->get();

		return view('ipkn',compact('section_1_header','section_1_body','section_2_header','section_2_body','section_3_header','section_4_header','section_5_header','section_6_header','capaian_ipkn','index'));
	}

	public function information(Request $request){
		$id = $request->id;

		$row = DB::table('ipkn_mst_indicator')->where('indicator_id',$id)->first();

		$html = '
				<table class="table table-borderless">
					<tbody>
						<tr>
							<td colspan="3"><span style="font-weight: bold">'.$row->indicator_code.' '.$row->indicator_name.'</span></td>
						</tr>

						<tr>
							<td>Deskripsi Indikator</td>
							<td>:</td>
							<td>'.$row->indicator_desc.'</td>
						</tr>
					
						<tr>
							<td>Principal</td>
							<td>:</td>
							<td>'.$row->indicator_principal.'</td>
						</tr>

						<tr>
							<td width="200px">Unit</td>
							<td width="10px">:</td>
							<td>'.$row->indicator_unit.'</td>
						</tr>
					
						<tr>
							<td>Rasio</td>
							<td>:</td>
							<td>'.$row->indicator_ratio.'</td>
						</tr>
					
						<tr>
							<td>Tipe Nilai</td>
							<td>:</td>
							<td>'.$row->indicator_type_value.'</td>
						</tr>
					
						<tr>
							<td>Pemilik</td>
							<td>: </td>
							<td>'.$row->indicator_owner.'</td>
						</tr>

						<tr>
							<td>Sumber</td>
							<td>:</td>
							<td><a target="_blank" href="'.$row->indicator_source.'">Buka</a></td>
						</tr>
					
						<tr>
							<td>Statistik Saat Ini</td>
							<td>:</td>
							<td>Min '.$row->indicator_min_value.'; Max '.$row->indicator_max_value.'; Median '.$row->indicator_med_value.'</td>
						</tr>
				
					</tbody>
				</table>
				';

		return $html;
	}
}
