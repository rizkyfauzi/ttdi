<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GlobalParameter extends Model
{
    protected $table = 'global_parameter';

    protected $guarded = [];
}
